import driver.WebDriverRunner;
import feature.ImportFeature;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import web.pages.HomePage;
import web.pages.TaskFivePage;

import static org.testng.Assert.assertTrue;

public class ImportTest extends BaseTest {
//public class ImportTest {

    @Test
    public void importTest() {
//        WebDriverManager.chromedriver().setup();
//        WebDriver driver = new ChromeDriver();
//        WebDriverRunner.setWebDriver(driver);
        new HomePage().selectTask("5");
        new TaskFivePage().clickInputBtn();
        assertTrue(new ImportFeature().importFileBySikuli());
    }
}

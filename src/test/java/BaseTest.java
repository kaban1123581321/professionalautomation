import driver.DriverConfig;
import driver.WebDriverFactory;
import driver.WebDriverRunner;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriverException;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.util.Objects;

import static driver.WebDriverRunner.getWebDriver;

public class BaseTest {

    private DriverConfig config = new DriverConfig();

    @BeforeMethod(alwaysRun = true, description = "Download web driver binaries")
    protected void beforeSuite(ITestContext context) {
//        try {
            WebDriverRunner.setWebDriver(new WebDriverFactory().createDriver(config));
//        } catch (WebDriverException e) {
//            WebDriverRunner.setWebDriver(new WebDriverFactory().createDriver(config));
//        }

        if (Objects.nonNull(System.getenv("CI_JOB_NAME"))) {
            getWebDriver().manage().window().setSize(new Dimension(1920, 1080));
        } else {
            getWebDriver().manage().window().maximize();
        }

    }

    @AfterMethod
    public void ensureDriverQuit() {
        if (getWebDriver() != null) {
            getWebDriver().quit();
        }
    }
}

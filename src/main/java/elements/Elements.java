package elements;

import driver.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.annotation.CheckReturnValue;
import javax.annotation.Nonnull;

public class Elements {

    @CheckReturnValue
    @Nonnull
    public static WebElement findByXpath(By xpathExpression) {
        return WebDriverRunner.getWebDriver().findElement(xpathExpression);
    }

    @CheckReturnValue
    @Nonnull
    public static WebElement findByCss(By seleniumSelector) {
        return WebDriverRunner.getWebDriver().findElement(seleniumSelector);
    }

    @CheckReturnValue
    @Nonnull
    public static WebElement findByXpath(String xpathExpression) {
        return WebDriverRunner.getWebDriver().findElement(By.xpath(xpathExpression));
    }

    @CheckReturnValue
    @Nonnull
    public static WebElement findByCss(String cssSelector) {
        return WebDriverRunner.getWebDriver().findElement(By.cssSelector(cssSelector));
    }
}

package elements;

import driver.WebDriverRunner;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.time.LocalDateTime;

import static driver.WebDriverRunner.resetImplicitTimeout;
import static driver.WebDriverRunner.setDefaultImplicitTimeout;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;
import static org.testng.Assert.fail;
import static utils.WaitUtils.timeout;

public class ElementAction {
    private static FluentWait<WebDriver> getFluentWait() {
        return new FluentWait<>(WebDriverRunner.getWebDriver())
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofMillis(300))
                .ignoring(StaleElementReferenceException.class);
    }

    public static void click(WebElement webElement) {
        LocalDateTime stopPoint = LocalDateTime.now().plusSeconds(30);
        while (stopPoint.isAfter(LocalDateTime.now())) {
            try {
                getFluentWait().until(elementToBeClickable(webElement)).click();
                break;
            } catch (ElementClickInterceptedException | StaleElementReferenceException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static boolean isElementNotPresent(WebElement element, int pollingDurationInSeconds) {
        resetImplicitTimeout();
        boolean isElementDisappeared = false;
        // minimum timeout depends on the timeout set in BasePage PageFactory.initElements
        FluentWait<WebDriver> wait = getFluentWait().withTimeout(Duration.ofSeconds(2));
        LocalDateTime spotPoint = LocalDateTime.now().plusSeconds(pollingDurationInSeconds);
        while (spotPoint.isAfter(LocalDateTime.now())) {
            try {
                wait.until(visibilityOf(element));
            } catch (NoSuchElementException | TimeoutException ex) {
                isElementDisappeared = true;
                break;
            }
        }
        setDefaultImplicitTimeout();
        return isElementDisappeared;
    }

    public static boolean isElementPresent(WebElement element, int pollingDurationInSeconds) {
        resetImplicitTimeout();
        boolean isElementPresent = true;
        // minimum timeout depends on the timeout set in BasePage PageFactory.initElements
        FluentWait<WebDriver> wait = getFluentWait().withTimeout(Duration.ofSeconds(2));
        LocalDateTime spotPoint = LocalDateTime.now().plusSeconds(pollingDurationInSeconds);
        while (spotPoint.isAfter(LocalDateTime.now())) {
            try {
                wait.until(visibilityOf(element));
                break;
            } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException ex) {
                isElementPresent = false;
            }
        }
        setDefaultImplicitTimeout();
        return isElementPresent;
    }

    public static WebElement getElement(WebElement element, int pollingDuration, String... exceptionMessage) {
        resetImplicitTimeout();
        boolean isElementReady = false;
        for (int i = 3; i >= 0; i--) {
            isElementReady = isElementPresent(element, pollingDuration);
            timeout(500L);
        }

        if (!isElementReady) {
            if (exceptionMessage.length > 0) {
                setDefaultImplicitTimeout();
                fail(exceptionMessage[0]);
            } else {
                setDefaultImplicitTimeout();
                fail("element was not found");
            }
        }

        setDefaultImplicitTimeout();
        return element;
    }
}

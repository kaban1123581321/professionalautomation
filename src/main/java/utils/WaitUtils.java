package utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class WaitUtils {

    public static void timeout(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
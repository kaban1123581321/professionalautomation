package driver;

import org.openqa.selenium.WebDriver;

import java.time.Duration;

public final class WebDriverRunner {
    public static final int DEFAULT_IMPLICIT_TIMEOUT = 20;
    private static final ThreadLocal<WebDriver> DRIVER_THREAD_LOCAL = new ThreadLocal<>();

    private WebDriverRunner() {
    }

    public static WebDriver getWebDriver() {
        return DRIVER_THREAD_LOCAL.get();
    }

    public static void setWebDriver(final WebDriver driver) {
        DRIVER_THREAD_LOCAL.set(driver);
    }

    public static void setDefaultImplicitTimeout() {
        getWebDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(DEFAULT_IMPLICIT_TIMEOUT));
    }

    public static void resetImplicitTimeout() {
        getWebDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(0));
    }
}

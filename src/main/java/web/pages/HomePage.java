package web.pages;

import driver.WebDriverRunner;

import static elements.ElementAction.click;
import static elements.Elements.findByXpath;

public class HomePage {


    public HomePage() {
        WebDriverRunner.getWebDriver().get("https://testingcup.pgs-soft.com/");
    }

    public void selectTask(String taskNumber){
        click(findByXpath(String.format("//a[@href='/task_%s']",taskNumber)));
    }
}

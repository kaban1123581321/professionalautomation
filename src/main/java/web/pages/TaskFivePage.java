package web.pages;

import org.openqa.selenium.By;

import static elements.ElementAction.click;
import static elements.Elements.findByXpath;

public class TaskFivePage {
    By inputBtn = By.xpath("//input[@type='file']/..");

    public void clickInputBtn(){
        click(findByXpath(inputBtn));
    }
}

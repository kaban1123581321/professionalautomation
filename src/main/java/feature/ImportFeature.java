package feature;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Finder;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import static data.Constants.RESOURCES_PATH;
import static utils.WaitUtils.timeout;

public class ImportFeature {

    public boolean importFileBySikuli() {
        boolean result = false;
        try {
//        Pattern inputFileImg = new Pattern(RESOURCES_PATH + "fileInput.PNG");


//        Screen screen = new Screen();
            Screen screen = new Screen();
            Pattern inputFileImg = new Pattern("C:\\Users\\bmenk\\IdeaProjects\\professionalAutomation\\src\\main\\resources\\fileInput.PNG");
            Pattern openButtonImg = new Pattern(RESOURCES_PATH + "Open.PNG");


            screen.wait(inputFileImg, 2);
            screen.paste(inputFileImg, RESOURCES_PATH + "testSikuli.txt");
//            screen.type(inputFileImg, resourcesPath+"testSikuli.txt");
            screen.click(openButtonImg);
            timeout(2000);
            String savedImgPath = RESOURCES_PATH + "sikuliImg.png";
            Pattern patternAlert = new Pattern(RESOURCES_PATH + "zle_sformatovane.PNG");
//            Pattern patternLogo = getPattern("logo.png");
            Pattern patternLogo = new Pattern(RESOURCES_PATH + "logo.png");
            screen.capture().save(RESOURCES_PATH, "sikuliImg.png");
            Finder finder = new Finder(screen.capture().getImage());
//            ImageIO.write(screen.capture().getImage(), "jpg", new File(savedImgPath));
//            Finder finder = new Finder(Image.create(savedImgPath));
            finder.find(patternAlert.similar(0.95));
//            finder.find(patternAlert.similar(1));
//            driver.switchTo().alert().accept();
//            byte[] bytes = ((ChromeDriver)driver).getScreenshotAs(OutputType.BYTES);
//            FileUtils.writeByteArrayToFile(new File(savedImgPath), bytes);
//            Finder finder = new Finder(Image.create(savedImgPath));
//            Settings.MinSimilarity = 0.95f;
//            finder.find(new Pattern(patternLogo));

            while (finder.hasNext()) {
                finder.destroy();
                result = true;
            }

//            Assert.assertTrue(result);
        } catch (FindFailed e) {
            e.printStackTrace();
        }
        return result;
    }
}
